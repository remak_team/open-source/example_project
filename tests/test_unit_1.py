import pytest


class TestCase:
    def test_class1_method_class1(self):
        from example_project import Class1
        _t = Class1()
        assert _t.method_class1(1, 2) == 3
        assert _t.method_class1(-6, 6) == 0
        with pytest.raises(AssertionError):
            _t.method_class1('a', 'b')


if __name__ == '__main__':
    t = TestCase()
    t.test_class1_method_class1()
