"""Example module"""
import argparse


class Class1:
    """This is an example of a docstrings

    Example
    -------
    >>> 1 + 2
    3
    >>> t = Class1()
    >>> t.method_class1(6, 5)
    11
    """

    def method_class1(self, a, b):
        """Calculates the sum of two arguments 'a' and 'b'

        Parameters
        ----------
        a : int
            first argument of a sum
        b : int
            second argument of a sum

        Returns
        -------
        x : int
            sum of 'a' and 'b'

        Raises
        ------
        AssertionError
            if 'a' or 'b' is not an instance of int
        """
        assert all([isinstance(x, int) for x in [a, b]])
        return a + b


def not_yet_implemented_function():  # pragma: no cover
    """Example of not finished function"""
    raise NotImplementedError


def main():  # pragma: no cover
    """Used as entry point for console app"""
    parser = argparse.ArgumentParser(prog='example_project')
    parser.add_argument(
        '-a', '--value-a', help='the value of a', type=int,
        default=6, nargs='?')
    parser.add_argument(
        '-b', '--value-b', help='the value of b', type=int,
        default=5, nargs='?')
    args = parser.parse_args()
    a = args.value_a
    b = args.value_b
    class_ = Class1()
    print(class_.method_class1(a, b))
