.. Remak Example Project documentation master file, created by
   sphinx-quickstart on Wed Jun 21 08:43:58 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Remak Example Project documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   human_doc/how_to.rst
   example_project/modules

Section
=======


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
