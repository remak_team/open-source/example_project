#! /usr/bin/env python
"""Distribution setup package.

Check `setup.cfg` for the project metadata
"""

__requires__ = ('setuptools>=30.3.0',)
"""rwt-compliant requirement list, means nothing unless invoked via `rwt` tool.

Ref: https://pypi.python.org/pypi/rwt#replacing-setup-requires
"""

import sys
from functools import wraps

import setuptools.config
from setuptools import setup
from setuptools.command.test import test as TestCommand
from setuptools.config import (
    ConfigMetadataHandler as SetupToolsConfigMetadataHandler,
    read_configuration as setuptools_read_configuration,
    configuration_to_dict as setuptools_configuration_to_dict
)


class PyTestCommand(TestCommand):
    """Custom command invoking pytest on `python -m setup test` call."""

    user_options = [('pytest-args=', 'a', 'Arguments to pass to pytest')]
    """Additional options list.

    a (str): arguments bypassed to `pytest` runner
    """

    def initialize_options(self):
        """Set initial option values."""
        super(PyTestCommand, self).initialize_options()
        self.pytest_args = []

    def finalize_options(self):
        """Reset final options state."""
        super(PyTestCommand, self).finalize_options()
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        """Execute pytest testrunner."""
        import pytest  # import here, 'cause outside the eggs aren't installed
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)


class DescriptionJoinerConfigMetadataHandler(SetupToolsConfigMetadataHandler):
    """Patched config processor, which provides ability to glue files within long description."""

    @property
    def parsers(self):
        """Monkey-patch and return option handlers dict."""
        parsers = super().parsers.copy()
        parsers['long_description'] = self._get_parser_compound(
            self._parse_list,
            lambda l: '\n'.join(
                map(self._parse_file, l)
            )
        )
        return parsers


# Monkey-patch setuptools until corresponding PR merged
# Ref: https://github.com/pypa/setuptools/pull/1131
setuptools.config.ConfigMetadataHandler = DescriptionJoinerConfigMetadataHandler


def _configuration_to_dict_dec(_configuration_to_dict):
    @wraps(_configuration_to_dict)
    def wrapper(*args, **kwargs):
        config = _configuration_to_dict(*args, **kwargs)
        options = config['options']
        metadata = config['metadata']
        try:
            if 'testing' not in options.get('extras_require', {}):
                tests_require = options['tests_require']
                if 'extras_require' not in options:
                    options['extras_require'] = {}
                options['extras_require']['testing'] = tests_require
        except KeyError:
            pass
        try:
            metadata['long_description'] = metadata[
                'long_description'].replace('.. :changelog:', '').lstrip()
        except KeyError:
            pass
        return config

    return wrapper


# Monkey-patch setuptools to mirror `tests_require` into `testing` extra
setuptools.config.configuration_to_dict = _configuration_to_dict_dec(
    setuptools_configuration_to_dict)

declarative_setup_params = setuptools_read_configuration('setup.cfg')

setup_params = {
    **declarative_setup_params['metadata'],
    **declarative_setup_params['options'],
    'cmdclass': {
        # Customise `python -m setup test` command to invoke pytest test runner
        'test': PyTestCommand,
    },
}

__name__ == '__main__' and setup(**setup_params)
